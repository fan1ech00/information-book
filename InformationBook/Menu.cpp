#include "Menu.h"
#include <iostream>
#include <conio.h>

#pragma warning( disable : 6031) // _getch

char* database_filename = (char*)"Worker.dat";

void Menu::printLoadFile()
{
	int result = workerManager.loadFile(database_filename);
	if (!result)
		printMessage("Workers successfully loaded from file.", Colors::COLOR_GREEN);
	else
		printMessage("Could not open file!", Colors::COLOR_RED);

	menuState = MenuStates::MAIN;
}

void Menu::printSaveFile()
{
	int result = workerManager.saveFile((char*)database_filename);
	if (!result)
		printMessage("Workers saved to file successfully.", Colors::COLOR_GREEN);
	else
		printMessage("Workers not saved to file!", Colors::COLOR_RED);

	menuState = MenuStates::MAIN;
}

void Menu::print()
{
	while (true) {
		switch (menuState)
		{
		case MenuStates::MAIN:
			printMain();
			break;
		case MenuStates::INFORMATION:
			printInformation();
			break;
		case MenuStates::ADD_WORKER:
			printAddWorker();
			break;
		case MenuStates::DEL_WORKER:
			printDelWorker();
			break;
		case MenuStates::SEARCH_SURNAME:
			printSearchSurname();
			break;
		case MenuStates::SEARCH_SALARY:
			printSearchSalary();
			break;
		case MenuStates::SORT_SURNAME:
			printSortSurname();
			break;
		case MenuStates::LOAD_FILE:
			printLoadFile();
			break;
		case MenuStates::SAVE_FILE:
			printSaveFile();
			break;
		default:
			break;
		}
	}
}

void Menu::printMessage(const char* text, int color)
{
	HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	system("cls");

	SetConsoleCursorPosition(hOut, { 1, 1 });
	SetConsoleTextAttribute(hOut, color);
	std::cout << text;

	SetConsoleCursorPosition(hOut, { 1, 2 });
	SetConsoleTextAttribute(hOut, Colors::COLOR_CYAN);
	std::cout << "Press any key!";
	_getch();
}

void Menu::printWorkers(Worker**& ptr, long long count)
{
	for (long long i = 0; i < count; i++) {
		std::cout << ptr[i]->getId() << ". " << ptr[i]->getName() << " " << ptr[i]->getSurname() << " "
			<< ptr[i]->getTelephone() << "  Pay->" << ptr[i]->getSalary() << std::endl;
	}
	delete[] ptr;
}

void Menu::printLogo()
{
	HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	system("cls");

	SetConsoleCursorPosition(hOut, { 20,1 });
	SetConsoleTextAttribute(hOut, Colors::COLOR_DARKGRAY);
	std::cout << "INFORMATION BOOK";

	SetConsoleCursorPosition(hOut, { 0,3 });
}

void Menu::printMain()
{
	printLogo();
	HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);

	COORD c = { 5, 3 };
	COORD _c = c;
	_c.Y++; SetConsoleCursorPosition(hOut, _c);
	std::cout << "1. Employee information";

	_c.Y++; SetConsoleCursorPosition(hOut, _c);
	std::cout << "2. Add worker";

	_c.Y++; SetConsoleCursorPosition(hOut, _c);
	std::cout << "3. Delete worker";

	_c.Y++; SetConsoleCursorPosition(hOut, _c);
	std::cout << "4. Search by surname";

	_c.Y++; SetConsoleCursorPosition(hOut, _c);
	std::cout << "5. Search by salary range";

	_c.Y++; SetConsoleCursorPosition(hOut, _c);
	std::cout << "6. Sort by surname";

	_c.Y++; _c.Y++; SetConsoleCursorPosition(hOut, _c);
	std::cout << "7. Loading information from file";

	_c.Y++; SetConsoleCursorPosition(hOut, _c);
	std::cout << "8. Writing information to a file";

	SetConsoleCursorPosition(hOut, c);
	SetConsoleTextAttribute(hOut, Colors::COLOR_CYAN);
	std::cout << "Choose action -> ";

	char buff[3]{};
	std::ios_base::sync_with_stdio(0);
	std::cin.ignore(std::cin.rdbuf()->in_avail());
	scanf_s("%3s", buff, 3);

	auto selection = atoi(buff);

	if (selection < (int)MenuStates::INFORMATION || selection > (int)MenuStates::SAVE_FILE) {
		printMessage("You entered an invalid value.", Colors::COLOR_RED);
	}
	else {
		menuState = (MenuStates)selection;
	}

	/*switch (atoi(buff))
	{
	case 1:
		menuState = MenuStates::INFORMATION;
		break;
	case 2:
		menuState = MenuStates::ADD_WORKER;
		break;
	case 3:
		menuState = MenuStates::DEL_WORKER;
		break;
	case 4:
		menuState = MenuStates::SEARCH_SURNAME;
		break;
	case 5:
		menuState = MenuStates::SEARCH_SALARY;
		break;
	case 6:
		menuState = MenuStates::SORT_SURNAME;
		break;
	case 7:
		menuState = MenuStates::LOAD_FILE;
		break;
	case 8:
		menuState = MenuStates::SAVE_FILE;
		break;
	default:
		printMessage("You entered an invalid value.", Colors::COLOR_RED);
		break;
	}*/
}

void Menu::printInformation()
{
	printLogo();

	Worker** ptr = nullptr;
	long long count;
	workerManager.getWorkers(ptr, &count);

	if (count > 0) {
		printWorkers(ptr, count);
		_getch();
	}
	else {
		printMessage("No workers in the RAM.", Colors::COLOR_RED);
	}

	menuState = MenuStates::MAIN;
}

void Menu::printAddWorker()
{
	printLogo();

	char char_buff[26];
	double double_buff;
	std::string str;

	int res;
	long long workerId = workerManager.addWorker();

	// name
	res = 1;
	do {
		std::cout << "Enter name: ";
		std::cin >> char_buff;
		if (std::cin.fail()) {
			std::cin.clear();
			std::cin.ignore(32767, '\n');
		}
		else {
			std::cin.ignore(32767, '\n');
			res = workerManager.workerSetName(workerId, char_buff);
		}

	} while (res);

	// surname
	res = 1; // reset res
	do {
		std::cout << "Enter surname: ";
		std::cin >> char_buff;
		if (std::cin.fail()) {
			std::cin.clear();
			std::cin.ignore(32767, '\n');
		}
		else {
			std::cin.ignore(32767, '\n');
			res = workerManager.workerSetSurname(workerId, char_buff);
		}

	} while (res);

	// telephone
	do {
		std::cout << "Enter telephone(format[(063)5635904]): ";
		std::cin >> char_buff;
		res = workerManager.workerSetTelephone(workerId, char_buff);
	} while (res);

	// salary
	do {
		std::cout << "Enter salary: ";

		std::cin >> double_buff;
		std::cin.clear();
		res = workerManager.workerSetSalary(workerId, double_buff);
	} while (res);

	menuState = MenuStates::MAIN;
	printMessage("Worker created successfully!", Colors::COLOR_GREEN);
}

void Menu::printDelWorker()
{
	printLogo();

	long long id;
	std::cout << "Enter worker ID: ";
	std::cin >> id;

	Worker* worker = workerManager.getWorker(id);
	if (worker != nullptr) {
		std::string str = "Do you really want to delete the worker ";
		str += worker->getName();
		str += " ";
		str += worker->getSurname();

		HWND hWnd = GetConsoleWindow();
		if (MessageBoxA(hWnd, str.c_str(), "Delete worker", MB_OKCANCEL) == 1) {
			workerManager.delWorker(id);
			printMessage("Worker deleted successfully!", Colors::COLOR_GREEN);
		}
	}
	else {
		printMessage("No worker with this id was found!", Colors::COLOR_RED);
	}

	menuState = MenuStates::MAIN;
}

void Menu::printSearchSurname()
{
	printLogo();

	std::cout << "Enter surname: ";
	char buff[30];
	scanf_s("%29s", buff, 29);

	Worker** ptr = nullptr;
	long long count;
	workerManager.getWorkers(buff, ptr, &count);
	if (count > 0) {
		printLogo();
		printWorkers(ptr, count);
		_getch();
	}
	else {
		printMessage("No workers found with this surname.", Colors::COLOR_RED);
	}

	menuState = MenuStates::MAIN;
}

void Menu::printSearchSalary()
{
	printLogo();

	double min;
	double max;
	std::ios_base::sync_with_stdio(0);
	std::cin.ignore(std::cin.rdbuf()->in_avail());
	std::cout << "Enter min salary: ";
	std::cin >> min;
	std::cout << "\nEnter max salary: ";
	std::cin >> max;

	if (min > max)
		std::swap(min, max);

	Worker** ptr = nullptr;
	long long count;
	workerManager.getWorkers(min, max, ptr, &count);
	if (count > 0) {
		printLogo();
		printWorkers(ptr, count);
		_getch();
	}
	else {
		printMessage("No workers found with this salary range", Colors::COLOR_RED);
	}

	menuState = MenuStates::MAIN;
}

void Menu::printSortSurname()
{
	auto size = workerManager.getSizeWorkers();
	if (size == 0) {
		printMessage("No workers in the database!", Colors::COLOR_RED);
	}
	else if (size == 1) {
		printMessage("Only one worker in the database!", Colors::COLOR_RED);
	}
	else {
		workerManager.sortBySurname();
		printMessage("Workers have been successfully sorted by surname.", Colors::COLOR_GREEN);
	}

	menuState = MenuStates::MAIN;
}
