#pragma once
#pragma warning( disable : 26495)

class Worker
{
	long long id; // personal identificator
	char* name;
	char* surname;
	char* telephone;
	double salary;
public:
	void setName(char* _name);
	void setSurname(char* _surname);
	void setTelephone(char* _telephone);
	void setSalary(double _salary);

	char* getName() const;
	char* getSurname() const;
	char* getTelephone() const;
	double getSalary() const;
	long long getId() const;

	// ������������ � ����������
	Worker();
	Worker(long long _id);
	Worker(long long _id, char* _name);
	Worker(long long _id, char* _name, char* _surname);
	Worker(long long _id, char* _name, char* _surname, char* _telephone);
	Worker(long long _id, char* _name, char* _surname, char* _telephone, double _salary);
	~Worker();
};