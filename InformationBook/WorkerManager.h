#pragma once
#include <vector>
#include "Worker.h"


class WorkerManager
{
	std::vector<Worker*> workers;
	static long long idInc; // incrementing identifier
	long long getWorkerIndx(long long _id) const;
public:
	long long addWorker();
	int delWorker(long long _id);

	int workerSetName(long long _id, char* _name);
	int workerSetSurname(long long _id, char* _surname);
	int workerSetTelephone(long long _id, char* _telephone);
	int workerSetSalary(long long _id, double _salary);

	Worker* getWorker(long long _id) const;
	long long getSizeWorkers();
	void getWorkers(Worker**& arr, long long* count) const;
	void getWorkers(char* _surname, Worker**& arr, long long* count) const;
	void getWorkers(double minSalary, double maxSalary, Worker**& arr, long long* count) const;

	int sortBySurname();
	int saveFile(char* filename);
	int loadFile(char* filename);
};