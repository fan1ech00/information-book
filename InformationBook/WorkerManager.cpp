#include "WorkerManager.h"
#include <Windows.h>
#include <fstream>

//#define tostring(X) # X

long long WorkerManager::idInc = 1;

long long WorkerManager::addWorker()
{
	long long id = idInc++;
	workers.push_back(new Worker(id));
	return id;
}

int WorkerManager::workerSetName(long long _id, char* _name)
{
	char* ptr = _name;
	while (*ptr) {
		if (!isalpha(*ptr))
			return 1;
		ptr++;
	}
	workers[getWorkerIndx(_id)]->setName(_name);
	return 0;
}

int WorkerManager::workerSetSurname(long long _id, char* _surname)
{
	char* ptr = _surname;
	while (*ptr) {
		if (!isalpha(*ptr))
			return 1;
		ptr++;
	}
	workers[getWorkerIndx(_id)]->setSurname(_surname);
	return 0;
}

int WorkerManager::workerSetTelephone(long long _id, char* _tel) // 7
{
	if (*_tel == '(' && *(_tel + 1) == '0') {
		int code = strtol(_tel + 2, NULL, 10);
		if (code == 39 || code == 67 || code == 68 || code == 96 || code == 97 || code == 98 // kyivstar
			|| code == 50 || code == 66 || code == 95 || code == 99 // vodafone
			|| code == 63 || code == 93) { // lifecell
			if (*(_tel + 4) == ')') {
				if (isdigit(*(_tel + 5)) && isdigit(*(_tel + 6)) && isdigit(*(_tel + 7)) && isdigit(*(_tel + 8))
					&& isdigit(*(_tel + 9)) && isdigit(*(_tel + 10)) && isdigit(*(_tel + 11)) ) {
					if (*(_tel + 12) == '\0') {
						workers[getWorkerIndx(_id)]->setTelephone(_tel);
						return 0;
					}

				}
			}
		}
	}
	return 1;
}

int WorkerManager::workerSetSalary(long long _id, double _salary)
{
	workers[getWorkerIndx(_id)]->setSalary(_salary);
	return 0;
}

Worker* WorkerManager::getWorker(long long _id) const
{
	long long index = getWorkerIndx(_id);
	if (index == -1)
		return nullptr;
	return workers[index];
}

long long WorkerManager::getSizeWorkers()
{
	return workers.size();
}

void WorkerManager::getWorkers(Worker**& arr, long long* count) const
{
	auto size = workers.size();
	*count = size;

	arr = new Worker*[size];

	for (unsigned long long i = 0; i < size; i++) {
		arr[i] = workers[i];
	}
}

void WorkerManager::getWorkers(char* _surname, Worker**& arr, long long* count) const
{
	auto sizeVector = workers.size();
	long long size = 0;

	for (long long i = 0; i < sizeVector; i++) {
		if (!lstrcmpiA(workers[i]->getSurname(), _surname)) {
			size++;
		}
	}

	*count = size;
	arr = new Worker* [size];

	int arrCount = 0;
	for (long long i = 0; i < sizeVector; i++) {
		if (!lstrcmpi(workers[i]->getSurname(), _surname)) {
			arr[arrCount] = workers[i];
			arrCount++;
		}
	}
}

void WorkerManager::getWorkers(double minSalary, double maxSalary, Worker**& arr, long long* count) const
{
	auto sizeVector = workers.size();
	long long size = 0;

	for (long long i = 0; i < sizeVector; i++) {
		double _salary = workers[i]->getSalary();
		if (_salary >= minSalary && _salary <= maxSalary) {
			size++;
		}
	}

	*count = size;
	arr = new Worker * [size];

	int arrCount = 0;
	for (long long i = 0; i < sizeVector; i++) {
		double _salary = workers[i]->getSalary();
		if (_salary >= minSalary && _salary <= maxSalary) {
			arr[arrCount] = workers[i];
			arrCount++;
		}
	}
}

int WorkerManager::sortBySurname()
{
	auto size = workers.size();
	if (size < 2)
		return 1;

	for (long long i = 0; i < size - 1; i++) {
		for (long long j = 0; j < size - i - 1; j++) {
			if (lstrcmpi(workers[j]->getSurname(), workers[j + 1]->getSurname()) > 0) {
				auto temp = workers[j];
				workers[j] = workers[j + 1];
				workers[j + 1] = temp;
			}
		}
	}

	return 0;
}

int WorkerManager::saveFile(char* filename)
{
	std::ofstream out(filename, std::ios::binary | std::ios::trunc | std::ios::out);
	if (out.is_open()) {
		auto vectorSize = workers.size();
		out.write((char*)&idInc, sizeof(idInc));
		out.write((char*)&vectorSize, sizeof(vectorSize));
		for (int i = 0; i < vectorSize; i++) {
			auto id = workers[i]->getId();
			out.write((char*)&id, sizeof(id));

			auto size = strlen(workers[i]->getName()) + 1;
			out.write((char*)&size, sizeof(size));
			out.write(workers[i]->getName(), size);

			size = strlen(workers[i]->getSurname()) + 1;
			out.write((char*)&size, sizeof(size));
			out.write(workers[i]->getSurname(), size);

			size = strlen(workers[i]->getTelephone()) + 1;
			out.write((char*)&size, sizeof(size));
			out.write(workers[i]->getTelephone(), size);

			auto salary = workers[i]->getSalary();
			out.write((char*)&salary, sizeof(salary));
		}

		out.close();
		return 0;
	}
	return 1;
}

int WorkerManager::loadFile(char* filename)
{
	std::ifstream in(filename, std::ios::binary | std::ios::in);
	if (in.is_open()) {
		char buff[26];
		double salary;
		long long id;
		long long size;
		auto vectorSize = workers.size();
		if (vectorSize > 0) {
			for (int i = 0; i < vectorSize; i++) {
				delete workers[i];
			}
			workers.clear();
		}

		in.read((char*)&idInc, sizeof(idInc)); // ���������� ������� ������� � ����������.
		in.read((char*)&vectorSize, sizeof(size)); // ���������� ������� ������� � ����������.
		if (vectorSize > 0) {
			workers.resize(vectorSize);
			for (int i = 0; i < vectorSize; i++) {
				in.read((char*)&id, sizeof(size));
				workers[i] = new Worker(id);

				in.read((char*)&size, sizeof size);
				in.read((char*)buff, size);
				workers[i]->setName(buff);

				in.read((char*)&size, sizeof size);
				in.read((char*)buff, size);
				workers[i]->setSurname(buff);

				in.read((char*)&size, sizeof size);
				in.read((char*)buff, size);
				workers[i]->setTelephone(buff);

				in.read((char*)&salary , sizeof(salary));
				workers[i]->setSalary(salary);
			}
		}

		in.close();
		return 0;
	}
	return 1;
}

int WorkerManager::delWorker(long long _id)
{
	long long index = getWorkerIndx(_id);
	if (index == -1)
		return 1;
	delete workers[index];
	workers.erase(workers.begin() + index);
	return 0;
}

long long WorkerManager::getWorkerIndx(long long _id) const
{
	auto size = workers.size();
	for (unsigned long long i = 0; i < size; i++) {
		if (workers[i]->getId() == _id) {
			return i;
		}
	}
	return -1;
}
