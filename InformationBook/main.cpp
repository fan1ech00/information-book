#include <iostream>
#include <Windows.h>
#include "Worker.h"
#include "Menu.h"

#define WINDOW_TITLE "InformationBook"
#define WINDOW_COLS 60
#define WINDOW_LINES 30

void cursorVisible(HANDLE hOut, bool isVisible);
void resizeWindow(int cols, int lines);

int main()
{
	HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	cursorVisible(hOut, false);
	//HANDLE hIn = GetStdHandle(STD_INPUT_HANDLE);
	//SetConsoleMode(hIn, ENABLE_MOUSE_INPUT | ENABLE_EXTENDED_FLAGS);

	SetConsoleTitleA(WINDOW_TITLE);
	resizeWindow(WINDOW_COLS, WINDOW_LINES);

	Menu menu;
	menu.print();

	system("pause");
}

void cursorVisible(HANDLE hOut, bool isVisible)
{
	CONSOLE_CURSOR_INFO cur;
	cur.bVisible = isVisible;
	cur.dwSize = 100;
	SetConsoleCursorInfo(hOut, &cur);
}

void resizeWindow(int cols, int lines)
{
	char str[30];
	wsprintf(str, "mode con cols=%d lines=%d", cols, lines);
	system(str);
}
