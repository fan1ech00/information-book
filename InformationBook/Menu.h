#pragma once
#include <Windows.h>
#include "WorkerManager.h"

enum class MenuStates {
	MAIN,
	INFORMATION, // ���� �� ���������
	ADD_WORKER,
	DEL_WORKER,
	SEARCH_SURNAME, // ����� �� �������
	SEARCH_SALARY,
	SORT_SURNAME, // ���������� �� �������
	LOAD_FILE,
	SAVE_FILE,
};

class Menu
{
	MenuStates menuState;
	WorkerManager workerManager;

	void printMessage(const char* text, int color);
	void printWorkers(Worker**& workers, long long count);
	void printLogo();

	void printMain();
	void printInformation();
	void printAddWorker();
	void printDelWorker();
	void printSearchSurname();
	void printSearchSalary();
	void printSortSurname();
	void printLoadFile();
	void printSaveFile();
public:
	void print();
};

namespace Colors {
	enum Colors {
		COLOR_BLACK = 0,
		COLOR_DARKBLUE = FOREGROUND_BLUE,
		COLOR_DARKGREEN = FOREGROUND_GREEN,
		COLOR_DARKCYAN = FOREGROUND_GREEN | FOREGROUND_BLUE,
		COLOR_DARKRED = FOREGROUND_RED,
		COLOR_DARKMAGENTA = FOREGROUND_RED | FOREGROUND_BLUE,
		COLOR_DARKYELLOW = FOREGROUND_RED | FOREGROUND_GREEN,
		COLOR_DARKGRAY = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE,
		COLOR_GRAY = FOREGROUND_INTENSITY,
		COLOR_BLUE = FOREGROUND_INTENSITY | FOREGROUND_BLUE,
		COLOR_GREEN = FOREGROUND_INTENSITY | FOREGROUND_GREEN,
		COLOR_CYAN = FOREGROUND_INTENSITY | FOREGROUND_GREEN | FOREGROUND_BLUE,
		COLOR_RED = FOREGROUND_INTENSITY | FOREGROUND_RED,
		COLOR_MAGENTA = FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_BLUE,
		COLOR_YELLOW = FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN,
		COLOR_WHITE = FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE,
	};
}
