#include "Worker.h"
#include <iostream>

void Worker::setName(char* _name)
{
	if (name != nullptr)
		delete[] name;
	name = new char[strlen(_name) + 1];
	strcpy_s(name, strlen(_name) + 1, _name);
}

void Worker::setSurname(char* _surname)
{
	if (surname != nullptr)
		delete[] surname;
	surname = new char[strlen(_surname) + 1];
	strcpy_s(surname, strlen(_surname) + 1, _surname);
}

void Worker::setTelephone(char* _telephone)
{
	if (telephone != nullptr)
		delete[] telephone;
	telephone = new char[strlen(_telephone) + 1];
	strcpy_s(telephone, strlen(_telephone) + 1, _telephone);
}

void Worker::setSalary(double _salary)
{
	salary = _salary;
}

char* Worker::getName() const
{
	return name;
}

char* Worker::getSurname() const
{
	return surname;
}

char* Worker::getTelephone() const
{
	return telephone;
}

double Worker::getSalary() const
{
	return salary;
}

long long Worker::getId() const
{
	return id;
}

Worker::Worker()
{
	id = 0;
	name = nullptr;
	surname = nullptr;
	telephone = nullptr;
	salary = 0;
}

Worker::Worker(long long _id)
{
	id = _id;
}

Worker::Worker(long long _id, char* _name) : Worker(_id)
{
	name = new char[strlen(_name) + 1];
	strcpy_s(name, strlen(_name) + 1, _name);
}

Worker::Worker(long long _id, char* _name, char* _surname) : Worker(_id, _name)
{
	surname = new char[strlen(_surname) + 1];
	strcpy_s(surname, strlen(_surname) + 1, _surname);
}

Worker::Worker(long long _id, char* _name, char* _surname, char* _telephone) : Worker(_id, _name, _surname)
{
	telephone = new char[strlen(_telephone) + 1];
	strcpy_s(telephone, strlen(_telephone) + 1, _telephone);
}

Worker::Worker(long long _id, char* _name, char* _surname, char* _telephone, double _salary) : Worker(_id, _name, _surname, _telephone)
{
	salary = _salary;
}

Worker::~Worker()
{
	if (name != nullptr)
		delete[] name;
	if (surname != nullptr)
		delete[] surname;
	if (telephone != nullptr)
		delete[] telephone;
}
